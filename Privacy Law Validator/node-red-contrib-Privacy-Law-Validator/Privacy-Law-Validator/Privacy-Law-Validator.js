

// to provide the module access to the Node-RED runtime API
module.exports = function(RED) {
    const request = require('request');
    
  

  
  //Reduce Granularity Node ([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})
  
              function PrivacyValidatorNode(config) {
                RED.nodes.createNode(this,config);
                var node = this;
                this.on('input', function(msg) {

                  node.PrivacyValidator = config.PrivacyValidator || "none";

              

              
               

               //Check the format of the recieved location
               var Location = msg.payload.split(",");
               if  (Location[0].match(/^(-?\d+(\.\d+)?)\s\s*(-?\d+(\.\d+)?)$/))
              {
                this.status({fill:"red",shape:"dot",text:"Privacy issue: fine-grained location"}); }       
    
              else if (Location[0].match(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([AZa-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/))
    
              {this.status({fill:"yellow",shape:"dot",text:"Partially reduced-location granularity"});}
              
             else if (Location[0].match(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/) && msg.topic == "City Name")
            {this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: Coarse-grained location"});}

            else if (msg.topic == "Coutry Name")
            {this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: Coarse-grained location"});}


            //Check the format of the heart rate 
            else if (msg.payload.includes("Average Heart Rate Status")|| msg.topic == "Heart Rate Status")
            {
              this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the heart rate beat has bean aggregated based on its status"});
            } //Check the format of data after applying Categoy based Aggregation (Status) to the Heart Rate Data 

            else if (msg.payload.includes("The average range of heart rate") || msg.topic == "Heart_Range")
            {
              this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the heart rate values have bean aggregated based on its range"});
            } //Check the format of data after applying Categoy based Aggregation (Range) to the Heart Rate Data in Blockly@rduino or in Node-RED


            else if (msg.payload.includes("Average heart rate for 5s") || msg.topic == "Minimised Heart Rate")
            {
              this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the heart rate values have been minimised"});
            } //Check the format of data after applying Minimise Raw Data Intake to the Heart Rate Data 


            else if (msg.topic.includes("Readings of Heart Rate per Minute"))
            {
              this.status({fill:"red",shape:"dot",text:"Privacy issue: heart rate data is sensitive data"});
            }//Check the format of the recieved data when sending raw format of data


          //Check the format of the recieved speed

          else if (msg.payload.includes("Speed range") || msg.topic == "Speed_Range")
          {
            this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the speed values have bean aggregated based on its range"});
          } //Check the format of data after applying Categoy based Aggregation (Range) to the Heart Rate Data in Blockly@rduino or in Node-RED


          else if (msg.payload.includes("Average speed") || msg.topic == "Minimised Speed per KM")
          {
            this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the speed values have been minimised"});
          } //Check the format of data after applying Minimise Raw Data Intake to the Heart Rate Data 


          else if (msg.topic.includes("Readings of Speed"))
          {
            this.status({fill:"red",shape:"dot",text:"Privacy issue: speed values have been sent in their raw format"});
          }//Check the format of the recieved data when sending raw format of data

                    //Check the format of the recieved distance

                    else if (msg.payload.includes("Distance range") || msg.topic == "Distance_Range")
                    {
                      this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the distance has bean aggregated based on its range"});
                    } //Check the format of data after applying Categoy based Aggregation (Range) to the Heart Rate Data in Blockly@rduino or in Node-RED
          
          
                    else if (msg.topic.includes("Total Distance"))
                    {
                      this.status({fill:"red",shape:"dot",text:"Privacy issue: Distance value have been sent in their raw format"});
                    }//Check the format of the recieved data when sending raw format of data

          

                    //Check the format of the recieved Calories

                    else if (msg.payload.includes("CAL range") || msg.topic == "Range of Total calories")
                    {
                      this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the Total Calories have bean aggregated based on its range"});
                    } //Check the format of data after applying Categoy based Aggregation (Range) to the Total calories in Blockly@rduino or in Node-RED
          
          
                    else if (msg.topic.includes("Total Calories"))
                    {
                      this.status({fill:"red",shape:"dot",text:"Privacy issue: Total Calories have been sent in their raw format"});
                    }//Check the format of the recieved data when sending raw format of data


                    //Check the format of the recieved Activity Spent Time

                    else if (msg.payload.includes("Time Range") || msg.topic == "Range of Activity Spent Time")
                    {
                      this.status({fill:"green",shape:"dot",text:"Complied with Privacy Law: the Activity Spent Time has bean aggregated based on its range"});
                    } //Check the format of data after applying Categoy based Aggregation (Range) to the Total calories in Blockly@rduino or in Node-RED
          
          
                    else if (msg.topic.includes("Activity Spent Time"))
                    {
                      this.status({fill:"red",shape:"dot",text:"Privacy issue: Activity Spent Time has been sent in their raw format"});
                    }//Check the format of the recieved data when sending raw format of data


                               //Check if the Date has been recieved


                               else if (msg.topic.includes("Date"))
                               {
                                 this.status({fill:"red",shape:"dot",text:"Privacy issue: personal data is recieved"});
                               }     
                     
                              //Check if the time has been recieved

                               else if (msg.topic.includes("Time"))
                               {
                                 this.status({fill:"red",shape:"dot",text:"Privacy issue: personal data is recieved"});
                               }

                               else if (msg.topic.includes("Time"))
                               {
                                 this.status({fill:"red",shape:"dot",text:"Privacy issue: personal data is recieved"});
                               }

               
                    //Check the format of the recieved age


                    else if (msg.payload.includes("Child") || msg.payload.includes("Adult") || msg.payload.includes("Senior")) {
                      this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: the age data has been anonymized through the categorization technique."});
                  } // Check the format of data after applying Data anonymization (Categorization) to the Heart Rate Data in Blockly@rduino or in Node-RED
                  
                  else if (/^\d+$/.test(msg.payload)) { // Check if payload is a number
                      this.status({fill:"red",shape:"dot",text:"Privacy issue: the age data is personal data and has been sent in its raw format."});
                  } // Check the format of data after applying Data anonymization (Categorization) to the Heart Rate Data
                  

         
           //Check the format of the recieved Name

           else if (msg.topic.includes("First Name") || msg.topic.includes("Second Name") )
           {
            this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: the name has been anonymized through the Pesduonmization technique"});
           }//Check the format of the recieved data when sending raw format of data

          //Check the format of the recieved Hieght

            else if (msg.payload.includes("Female") || msg.payload.includes("Male"))
            {
              this.status({fill:"red",shape:"dot",text:"Privacy issue: gender information is personal data and has been sent in its raw format."});
            }
 
            else if (msg.payload.includes("G1") || msg.payload.includes("G2"))
            {
             this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: the gender data has been anonymized through the generalization technique."});
            }


           //Check the format of the recieved Hieght

           else if (msg.topic.includes("Height"))
           {
             this.status({fill:"red",shape:"dot",text:"Privacy issue: height data is personal data and has been sent in its raw format."});
           }

           else if (msg.payload.includes("Short") || msg.payload.includes("Average") || msg.payload.includes("Tall"))
           {
            this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: the height data has been anonymized through the categorization technique."});
           }

          //Check the format of the recieved Wieght

          else if (msg.topic.includes("Weight"))
          {
            this.status({fill:"red",shape:"dot",text:"Privacy issue: weight data is personal data and has been sent in its raw format."});
          }

          else if (msg.topic.includes("Wieght"))
          {
            this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: weight data has been anonymized through the randomization technique."});
          }

          //Check the format of the recieved Medical Records

          else if (msg.topic.includes("Medical Records"))
          {
            this.status({fill:"red",shape:"dot",text:"Privacy issue: medical records are sensitive data"});
          }

          else if (msg.topic.includes("Randomised medical records"))
          {
           this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: medical records have been anonymized through the randomization technique."});
          }//Check the format of the recieved data when sending raw format of data

          //Check the format of the disability status

          else if (msg.payload.includes("Not Disabeled"))
          {
            this.status({fill:"red",shape:"dot",text:"Privacy issue: disability status is sensitive data"});
          }//Check the format of the recieved data when sending raw format of data

          else if (msg.payload.includes("Not D"))
          {
           this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: the disability status has been anonymized through the gendereralization technique."});
          }//Check the format of the recieved data when sending raw format of data

            //Check the format of the telephone number


          else if (msg.payload == /(\d{3})(\d{3})(\d{4})/)
          {
            this.status({fill:"red",shape:"dot",text:"Privacy issue: telephone number is personal data and has been sent in its raw format."});
          }

          else if (msg.payload == "XXX-XXX-$3")
          {
            this.status({fill:"green",shape:"dot",text:"Compliant with Privacy Law: telephone number has been anonymized through the generalization technique."});
          }

 
          });
      }
      RED.nodes.registerType("privacy-law-validator",PrivacyValidatorNode);
  }
  
  
  
