module.exports = function(RED) {
    function EncryptedDataCommunicationNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        // Retrieve the node configuration
        node.apiKey = config.apiKey;
        node.url = config.url;

        node.on('input', function(msg) {
            // Use the message payload as data
            var data = msg.payload;

            // Prepare the HTTP request
            const https = require('https');
            const url = new URL(node.url);

            const options = {
                hostname: url.hostname,
                port: url.port || 443,
                path: url.pathname,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${node.apiKey}`
                }
            };

            const req = https.request(options, (res) => {
                let responseData = '';

                res.on('data', (chunk) => {
                    responseData += chunk;
                });

                res.on('end', () => {
                    msg.payload = responseData;
                    node.send(msg);
                });
            });

            req.on('error', (e) => {
                node.error(`Problem with request: ${e.message}`);
            });

            // Write data to request body
            req.write(JSON.stringify(data));
            req.end();
        });
    }

    RED.nodes.registerType("encrypted-data-communication", EncryptedDataCommunicationNode, {
        defaults: {
            name: { value: "" },
            apiKey: { value: "" },
            url: { value: "" }
        },
        credentials: {
            apiKey: { type: "password" }
        }
    });
};
