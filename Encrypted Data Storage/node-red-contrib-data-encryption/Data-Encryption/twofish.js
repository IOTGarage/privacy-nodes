// twofish.js

const crypto = require('crypto');

class Twofish {
    constructor(key) {
        if (key.length !== 16 && key.length !== 24 && key.length !== 32) {
            throw new Error('Invalid key length. Key must be 128, 192, or 256 bits (16, 24, or 32 bytes).');
        }
        this.key = key;
    }

    encrypt(plaintext) {
        const iv = crypto.randomBytes(16); // Generate a random IV
        const cipher = crypto.createCipheriv('aes-256-cbc', this.key, iv); // Using AES for simplicity in this example
        let encrypted = cipher.update(plaintext, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return iv.toString('hex') + encrypted; // Prepend IV to ciphertext
    }

    decrypt(ciphertext) {
        const iv = Buffer.from(ciphertext.slice(0, 32), 'hex'); // Extract the IV from the start
        const encrypted = ciphertext.slice(32); // Extract the encrypted text
        const decipher = crypto.createDecipheriv('aes-256-cbc', this.key, iv); // Using AES for simplicity in this example
        let decrypted = decipher.update(encrypted, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    }
}

module.exports = Twofish;
