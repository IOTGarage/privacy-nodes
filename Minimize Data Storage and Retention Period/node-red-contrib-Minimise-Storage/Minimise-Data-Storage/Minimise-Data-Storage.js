module.exports = function(RED) {
  function MinimiseDataStorageNode(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    node.on('input', function(msg) {
      const action = config.MinimiseDataStorage;
      const retentionPeriod = config.dataRetentionPeriod;
      const currentTime = new Date().getTime();
      const dataKey = 'dataStorage';

      if (!msg[dataKey]) {
        msg[dataKey] = {};
      }

      if (action === 'Append to File') {
        if (!msg[dataKey].fileData) {
          msg[dataKey].fileData = [];
        }
        msg[dataKey].fileData.push({
          timestamp: currentTime,
          data: msg.payload
        });
      } else if (action === 'Create a new File') {
        msg[dataKey].fileData = [{
          timestamp: currentTime,
          data: msg.payload
        }];
      }

      msg[dataKey].fileData = msg[dataKey].fileData.filter(record => {
        const recordAge = (currentTime - record.timestamp) / (1000 * 60 * 60 * 24);
        return recordAge <= retentionPeriod;
      });

      node.send(msg);
    });
  }

  RED.nodes.registerType("minimise-data-storage", MinimiseDataStorageNode);
};


/*module.exports = function(RED) {
  const fs = require('fs');
  const path = require('path');

  function MinimizeDataStorageNode(config) {
      RED.nodes.createNode(this, config);
      var node = this;

      // Retrieve node configuration
      node.name = config.name;
      node.action = config.MinimizeDataStorage;
      node.retentionPeriod = parseInt(config.dataRetentionPeriod, 10); // in days

      node.on('input', function(msg) {
          var data = msg.payload;
          var fileName = "data_storage.txt";
          var filePath = path.join(__dirname, fileName);

          // Implement the action (append or create)
          if (node.action === 'create') {
              fs.writeFile(filePath, data, function(err) {
                  if (err) {
                      node.error("Error writing to file: " + err);
                  } else {
                      node.log("Data written to new file");
                  }
              });
          } else if (node.action === 'append') {
              fs.appendFile(filePath, data, function(err) {
                  if (err) {
                      node.error("Error appending to file: " + err);
                  } else {
                      node.log("Data appended to file");
                  }
              });
          }

          // Implement data retention logic
          const currentTime = new Date().getTime();
          const retentionTime = node.retentionPeriod * 24 * 60 * 60 * 1000; // Convert days to milliseconds

          fs.stat(filePath, function(err, stats) {
              if (err) {
                  node.error("Error getting file stats: " + err);
              } else {
                  const fileAge = currentTime - new Date(stats.mtime).getTime();
                  if (fileAge > retentionTime) {
                      // File is older than the retention period, delete it
                      fs.unlink(filePath, function(err) {
                          if (err) {
                              node.error("Error deleting old file: " + err);
                          } else {
                              node.log("Old file deleted due to retention policy");
                          }
                      });
                  }
              }
          });

          node.send(msg);
      });
  }

  RED.nodes.registerType("minimize-data-storage", MinimizeDataStorageNode);
};
*/