# Privacy Nodes



## Description
Privacy Nodes are privacy components designed according to Privacy by Design (PbD) guidelines and the Combined Privacy Law Framework (CPLF). They assist software developers in integrating privacy-preserving measures into their applications while enhancing awareness of privacy practices and compliance with data protection laws.

## Design and Implementation of Privacy-Preserving Components

- **Reduce Location Granularity**: Granularity refers to the level of detail represented by the data. High granularity provides a detailed view, while low granularity offers a summarized view. For instance, a full address is fine-grained and carries more privacy risks compared to coarse-grained data like a city or country name.
- **Minimize Raw Data Intake**: This privacy node reduces the amount of raw data it receives by calculating the average of the sensor data values. It offers options to minimise different types of sensor data, such as heart rate or speed values.
- **Category-based Aggregation**: This privacy node reduces the granularity of the message payloads of users' data. For example, instead of using exact values (e.g., 160 CAL), the burned calories of a particular workout can be represented as 150-200 CAL.
- **Minimize Data Storage and Retention Period**: This privacy node minimizes the amount of data stored by an IoT application and the duration for which data is retained. It allows you to either append data to an existing file or create a new file and specifies the retention period in days. This helps in ensuring that data is not stored longer than necessary, reducing the risk of privacy violations.
- **Access Control**: The Access Control node is designed to enforce access control mechanisms based on specified criteria. The node expects the input message to be formatted in a specific way to perform the authorization checks correctly.
- **Data Anonymization**: This privacy node suggests different data anonymization techniques for anonymizing personally identifiable information before the data gets used by IoT applications.
- **Encrypted Data Storage**: This privacy node suggests encryption techniques based on type. IoT applications should store data in encrypted form to prevent privacy violations from attacks and unauthorized access.</p>
- **Encrypted Data Communication**: This privacy node suggests different components in an IoT application should consider encrypted data communication wherever possible. Encrypted data communication reduces the potential privacy risks due to unauthorized access during data transfer between components. This node sends data to the specified URL using HTTPS and includes the provided API key for authentication.
- **Privacy Law Validator**: This privacy node checks the status of the data it receives to determine if it complies with Privacy and Data Protection laws.




## Usage

### Node-RED:

- To run the Node-RED locally, follow the steps [here](https://nodered.org/docs/getting-started/local).
- Download Canella-Node-RED folder.
#### Add Privacy Nodes to your Node-RED:
- Cd (path of the new created node) 
- Npm link 
- cd ~/.node-red 
- npm link  (name of the created node) e.g., node-red-contrib-reduce 
- Node-red 
- In Node-RED, you can find these Privacy Nodes in the Node-RED palette under the Privacy Nodes category.


### Examples

Example configurations and usage scenarios can be found in the documentation and the provided demo video [here](https://www.youtube.com/watch?v=7V0q3MyAzCQ&t=75s).


## Prototyping: Fitness Tracking IoT Application

A hypothetical use case scenario demonstrates how Canella helps integrate privacy-preserving components into the data flow of a Fitness Tracking IoT application.  It is a helpful IoT-based system that enables trainees to track their activities easily. Additionally, it assists trainers in managing their trainees, easily monitoring their progress, and encouraging them for their health, wellness, and safety.

![Fitness Tracking IoT Application](./images/UseCase.jpg)

## Canella
Canella is an integrated IoT development ecosystem designed to prioritize privacy in IoT applications. It leverages End-User Development (EUD) tools such as Blockly@rduino and Node-RED to help developers create end-to-end IoT applications that comply with privacy regulations. Canella provides real-time feedback on privacy concerns, streamlines privacy implementation, and reduces cognitive load, thus enhancing productivity and privacy awareness among developers.

### Link to Canella for Prototyping
- For detailed insights into the Canella tool, including its source code and documentation, visit the link to the <a href="https://gitlab.com/IOTGarage/canella" title="Canella Repository"> Canella Repository. </a>



### Link to Privacy Blocks
- Detailed insights into the Privacy Blocks are available at the <a href="https://gitlab.com/IOTGarage/privacy-blocks" title="Privacy Blocks Repository"> Privacy Blocks Repository. </a>
